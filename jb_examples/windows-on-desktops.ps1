# Skript to initialize my Virtual Desktops 
# Needs Module VirtualDesktop
# https://www.powershellgallery.com/packages/VirtualDesktop/

Import-Module -Name VirtualDesktop -ErrorAction SilentlyContinue -WarningAction SilentlyContinue

# Begin on Desktop 0 (Browser)
Start-Process firefox
sleep 3

# Goto Desktop 1 (Dateien)
Switch-Desktop 1
Start-Process explorer
sleep 3

# Goto Desktop 1 (Konsolen)
Switch-Desktop 2
Start-Process wt
sleep 3

# Back to Desktop 0 - Begin
Switch-Desktop 0


# Variables
$AppLocalNeovim = "$HOME\AppData\Local\nvim"
$AppDataNeovim  = "$HOME\AppData\Local\nvim-data"
$ProgNiFolder   = "C:\programme_ni"
$NvimFolder     = "$ProgNiFolder\nvim-win64"
$NvimQtExe      = "$ProgNiFolder\nvim-win64\bin\nvim-qt.exe"
$NvimExe        = "$ProgNiFolder\nvim-win64\bin\nvim.exe"
$ScriptFolder   = (Get-Location).Path 

# URL to stabil Neovim Release
$Url            = "https://github.com/neovim/neovim/releases/download/stable/nvim-win64.zip"
# Resulting Nvim Zip Path
$NvimZipPath    = "$ProgNiFolder\nvim-win64.zip"


"Neovim for Windows PowerShell Environment"
"========================================="

$reset = Read-Host -Prompt "Delete/Reset all Neovim Windows Stuff? (y / n)"
if ($reset -eq "y") {
	if (Test-Path $AppLocalNeovim) {
		Remove-Item -Path $AppLocalNeovim -Recurse -Force
		"Deleted App Local Folder $AppLocalNeovim"
	}
	if (Test-Path $AppDataNeovim) {
		Remove-Item -Path $AppDataNeovim -Recurse -Force
		"Deleted App Data Folder $AppDataNeovim"
	}
	if (Test-Path $NvimFolder) {
		Remove-Item -Path $NvimFolder -Recurse -Force
		"Deleted Nvim Folder $NvimFolder"
	}
}

# Path C:\programme_ni check/install
"Check/Create C:\programme_ni"
if ( $(Test-Path $ProgNiFolder) -eq $false ) {
    mkdir $ProgNiFolder
}

"CD in C:\programme_ni"
Set-Location $ProgNiFolder

# Download Stabel Release
if (Test-Path -Path $NvimZipPath) {
	$download = Read-Host -Prompt "Zip already available - New Download? (y / n)"
	if ($download -eq "y") {
		"Download Stable Release Neovim (approx. 12 MiB)"
		Invoke-WebRequest -URI $Url -OutFile $NvimZipPath -UseBasicParsing
	} 
} else {
	"Download Stable Release Neovim (approx. 12 MiB) already"
	Invoke-WebRequest -URI $Url -OutFile $NvimZipPath -UseBasicParsing
}

# use unzip Tool from Git
"Unzip / Expand-Archive for Zip File $NvimZipPath (stable Neovim Release)"
# Unzip needs full Git/Linux Tools env
# unzip -qq .\nvim-win64.zip 
# else use Expand-Archive -Path nvim-win64.zip -DestinationPath C:\programme_ni\nvim-win64
# Expand-Archive -Path $NvimZipPath -DestinationPath $ProgNiFolder
7z x $NvimZipPath

if ( Test-Path $NvimQtExe ) {
	"Resulting Neovim Qt Executable: $NvimQtExe"
	"Resulting Neovim CLI Executable: $NvimExe"
	"Will later become Aliases in my Profiles!"
}

# Prepare Neovim Config Windows			
# ~/AppData/Local/nvim/init.vim	(or init.lua)
if ( $(Test-Path $AppLocalNeovim) -eq $false ) {
    mkdir $AppLocalNeovim -Force | Out-Null
	"Created $AppLocalNeovim"
}
"Check/Create Config Folder Neovim: $AppLocalNeovim"
"Info for Neovim Data on Windows OS: $AppDataNeovim"


# Copy the prepared NVIM Configuration from Subfolder .config\nvim
Set-Location $ScriptFolder
Copy-Item -Path ".\.config\nvim\*" -Destination "$AppLocalNeovim" -Recurse
"I copied the prepared Config for Nvim with lazy.nvim"
$firststart = Read-Host -Prompt "Do you want to start Neovim for the first time? (y / n)"
if ($firststart) {
	& $NvimExe
}
Write-Host -ForegroundColor Red "`nTool nvim will work with new PowerShell Session!`n"



# NO MORE LazyVim - Just the ordinary lazy.nvim Plugin Tech
# =========================================================
#
#"LazyVim Config for Neovim:"
#"==================================================="
## the LazyVim Starter provided by Folke:
#$lazyvim = Read-Host -Prompt "Do you want FRESH LazyVim Config for Neovim (y / n)"
#"# Delete (or move) existing nvim-folder:"
#if ($lazyvim -eq "y") {
#	Remove-Item -Path $HOME\AppData\Local\nvim -Recurse -Force
#	git clone https://github.com/LazyVim/starter $HOME\AppData\Local\nvim
#
#} else {
#	"OK - no Lazyvim now!"
#}
# =========================================================

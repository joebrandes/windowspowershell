#      _                ____
#     | |  ___    ___  | __ )
#  _  | | / _ \  / _ \ |  _ \
# | |_| || (_) ||  __/ | |_) |_
#  \___/  \___/  \___| |____/(_)

# Version: 06.02.2022

# Skript to initialize my Virtual Desktops aka Workspaces
# Desktop Numbers from 0 to 8 (means 1 to 9 for humans ;-)
# in my case:
# 0 - Browser
# 1 - Dateien (Files)
# 2 - Konsolen (Terminals)
# 3 - Coding (mostly: MS Code or Vim/Neovim)
# 4 - VMs (Hyper-V, VirtualBox, KVM/qemu)
# 5 - Office
# 6 - E-Mail
# 7 - Graphics
# 8 - System (in Windows: MobaXterm with X-Server for WSL2 Gui)

# Needs Module VirtualDesktop
# https://www.powershellgallery.com/packages/VirtualDesktop/

# e.g. as link on Desktop with something like: (May cause Problems!!!)
# C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -noprofile -WindowStyle Hidden -file "C:\Users\joeb\Documents\WindowsPowerShell-Skripte\windows-on-9-desktops.ps1"
# with Responses in PS-Console:
# C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -noprofile -file "C:\Users\joeb\Documents\WindowsPowerShell-Skripte\windows-on-9-desktops.ps1"

# Load the VirtualDesktop Module
# Should be managed via if condition to exit Script with no module available!
Import-Module -Name VirtualDesktop -ErrorAction SilentlyContinue -WarningAction SilentlyContinue

# Begin on Desktop 0 (Browser)
# Switch-Desktop 0
# Firefox not a good idea - most probably because of lots of processes 
# see child processses - FF then always on next Desktop!
# Start-Process firefox
# use https://vieb.dev/ instead for all the vim/neovim Freaks out there
# Vieb (as portable) is attached to PowerShell Shell!
#if (Test-Path -Path C:\programme_ni\Vieb\Vieb.exe)
#{
#	Start-Process -FilePath C:\programme_ni\Vieb\Vieb.exe
#}
#else 
#{
#	$null = Read-Host 'You have no Vieb Browser - see: vieb.dev - Press any Key.#..'
#}

# or the portable Chrome
if (Test-Path -Path C:\programme_ni\PortableApps\GoogleChromePortable\GoogleChromePortable.exe)
{
	Start-Process -FilePath C:\programme_ni\PortableApps\GoogleChromePortable\GoogleChromePortable.exe
}
else 
{
	$null = Read-Host 'You have no Chrome Browser - see: PortableApps - Press any Key...'
}



# Give Browser time to start
sleep 5

# For testing:
# pause

# Goto Desktop 1 (Dateien)
Switch-Desktop 1
# sleep 2
# Start Windows Explorer
Start-Process explorer
sleep 3

# Goto Desktop 2 (Konsolen)
Switch-Desktop 2
# sleep 2
# Start Windows Terminal
Start-Process wt
sleep 4

# Goto Desktop 8 (System)
Switch-Desktop 8
# sleep 2
# ssh-Client and X-Server for WSL2 Gui Programms
# MobaXterm may throw errors in Terminal
$ssh = "C:\programme_ni\MobaXterm_Portable_v21.5\MobaXterm_Personal_21.5.exe"
$null = Start-Process -FilePath $ssh
sleep 6

# Back to Desktop 0 - Browser
Switch-Desktop 0
# Start Windows Explorer
# Start-Process explorer
# sleep 3

# Exit and Close PS Window
# Exit 1 - as Code for Standard Terminal-Scripts
[Environment]::Exit(1)


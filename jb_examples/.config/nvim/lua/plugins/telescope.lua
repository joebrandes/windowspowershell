return {
  -- Telescope stuff
  'nvim-telescope/telescope.nvim', tag = '0.1.8',
  -- or                              , branch = '0.1.x',
  config = function()
    require('telescope').setup{
      defaults = {
        file_ignore_patterns = {
            ".local/share/.*",
            ".cache/.*",
            ".config/libreoffice/.*",
            ".config/Code/.*",
            ".vscode/.*",
            "node_modules/.*",
            ".venvs/.*",
            "%.env",
            "yarn.lock",
            "package%-lock.json",
            "lazy%-lock.json",
            "init.sql",
            "target/.*",
            ".git/.*",
        },
      },
    }
  end,
  dependencies = { 'nvim-lua/plenary.nvim' }
}

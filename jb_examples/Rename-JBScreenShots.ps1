<#
     _            ____    ____                 _
    | | ___   ___| __ )  / ___|  ___ _ __ ___ (_)_ __   __ _ _ __ ___
 _  | |/ _ \ / _ \  _ \  \___ \ / _ \ '_ ` _ \| | '_ \ / _` | '__/ _ \
| |_| | (_) |  __/ |_) |  ___) |  __/ | | | | | | | | | (_| | | |  __/
 \___/ \___/ \___|____/  |____/ \___|_| |_| |_|_|_| |_|\__,_|_|  \___|

#>

$directories = Get-ChildItem -Directory

foreach ($dir in $directories) {
    # print Directory
    "Directory: " + $dir

    # get files from Dir
    $subdirfiles = Get-ChildItem -File -Path $dir 

    foreach ($subdirfile in $subdirfiles) {
        
        # time last write in my own timestamp format - like: 20200401-150034
        $timestamp = $subdirfile.LastWriteTime.ToString('yyyyMMdd-HHmmss')
        $extension = $subdirfile.Extension

        # new names please
        Rename-Item -Path "$dir\$subdirfile" -NewName "$timestamp-$dir$extension"
    }

}


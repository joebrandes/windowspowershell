<h3>
<img src="https://gitlab.com/joebrandes/windowspowershell/-/raw/master/jb_examples/PowerShell_5.0_icon-64px.png">
WindowsPowerShell Profile 2025
</h3>

<p>
Impressions with Tech-Examples...
<img style="margin: 20px 0px;" src="https://gitlab.com/joebrandes/windowspowershell/-/raw/master/jb_examples/JB-WindowsPowerShell-2025.jpg" style="margin: 20px 0px"> </br>
Windows Terminal mit Windows Powershell und Tools (Eza, Vifm, Nvim, Fzf)
</p>

**TOC**

- [📣 Intro](#-intro)
- [🛠️ jb\_tools (Folder for CLI Tools)](#️-jb_tools-folder-for-cli-tools)
- [🗃️ jb\_examples (Folder for Script-Examples / Neovim Installation)](#️-jb_examples-folder-for-script-examples--neovim-installation)
- [📰 jb\_fonts (Folder for Fonts)](#-jb_fonts-folder-for-fonts)
- [💻 Windows Terminal](#-windows-terminal)
  - [🖼️ NordTheme](#️-nordtheme)
- [📝 VS Code - Example settings.json](#-vs-code---example-settingsjson)

## 📣 Intro

Ideas and Public Availability for Trainees and everybody else.

    You can check out Pre-2025 WindowsPowerShell Profile with git checkout v1.0

    Please do this outside of WindowsPowerShell - Git Bash for example

Ideas for Rebuild of WindowsPowerShell Profile

*   Slim down the use of Modules to speed up launch of WindowsPowerShell 
    with factor 5 to 10 (!)

*   Cross-Platform Avaibility: Provide CLI Tools with support for various OS/Shell-Platforms

*   Use `.env` file for your Profile-Configuration 

    Without changing the `$PROFILE` file - which is not a good idea because 
    of Git-Status for `$PROFILE` - you can switch Variables to your liking.

    ```ini 
    JBShowHeader=OFF
    JBMessages=OFF
    JBPowerlinePrompt=starship
    JBPoshTheme=jb-nord
    ```

    The `.env` will be Git-ignored.

*   Later/To-Do: Everything switchable with PowerShell (Core for Linux):
    `jb_tools` vs. `jb_tools_linux`



## 🛠️ jb_tools (Folder for CLI Tools)

A short Overview of the Tools we use:

<details>
<summary><strong>OhMyPosh</strong> - 
previously via Module - now: Standalone (with no install)</summary>

*   [OhMyPosh Website](https://ohmyposh.dev/)
*   [OhMyPosh GitHub](https://github.com/jandedobbeleer/oh-my-posh) - 
*   [OhMyPosh Releases](https://github.com/JanDeDobbeleer/oh-my-posh/releases)

*   Themes: `get-poshThemes -Path .\jb_tools\posh-themes\` or 

*   look online via
    [OhMyPosh Themes](https://ohmyposh.dev/docs/themes)

</details>

<details>
<summary><strong>Zoxide</strong> - <code>z</code> the better cd / autojump / 
Zlocation with FZF (<code>zi</code>)</summary>

*   [Website Zoxide](https://crates.io/crates/zoxide)
*   [Zoxide GitHub](https://github.com/ajeetdsouza/zoxide)

Subfolder with Zoxide Tech

</details>

<details>
<summary><strong>Vifm</strong> - Vi-like File Manager with Theming, Previews</summary>

*   [Vifm Website](https://vifm.info/)
*   [Vifm GitHub](https://github.com/vifm/vifm)

</details>


<details>
<summary><strong>FZF</strong> - Fuzzy Finder - no Terminal without it</summary>

*   Single File: <code>fzf.exe</code>
*   [FZF GitHub](https://github.com/junegunn/fzf)

</details>

<details>
<summary><strong>Eza</strong> - better ls with Icons, Trees and Git Support</summary>

*   Single File: <code>eza.exe</code>
*   [Website eza.rocks](https://eza.rocks/)
*   [Eza GitHub](https://github.com/eza-community/eza)

*   Theming / Config: <code>.\eza\theme.yml</code> (Dummy / Empty)

</details>

<details>
<summary><strong>Bat</strong> - better cat</summary>

*   Single File: <code>bat.exe</code> - in Debian <code>batcat</code> (!) 
*   [Bat GitHub](https://github.com/sharkdp/bat)

</details>

<details>
<summary><strong>Fastfetch</strong> - the modern Neofetch for every OS/Shell</summary>

*   [Fastfetch GitHub](https://github.com/fastfetch-cli/fastfetch)

</details>

<details>
<summary><strong>JPEGView</strong> - the standalone Preview for Windows</summary>

*   from Project [GitHub JPEGView](https://github.com/sylikc/jpegview)

</details>

<details>
<summary><strong>7z</strong> - single exe from 7-Zip Tools for speed</summary>

*   [Website 7-zip.org](https://www.7-zip.org/)

</details>

<details>
<summary><strong>starship.toml</strong> - prepared <code>starship.toml</code> 
for later Starship Installation, if we wish.</summary>

*   [Starship Website](https://starship.rs)

I am pretty happy with **OhMyPosh** these days.

</details>


## 🗃️ jb_examples (Folder for Script-Examples / Neovim Installation)

This folder contains various stuff:

*   **Neovim** Configuration with **lazy.nvim** to start the Neovim/VIm Experience

    Install-Script for Neovim (Zip-Tech/No-Install): `Install-JBNeovim.ps1`

*   Backgrounds - Pics (mostly for Nord Theme)
*   VS Code stuff
*   Div Scripts and Pics



## 📰 jb_fonts (Folder for Fonts)

Easy Install on Windows via Marking Fonts and Install via Contextmenu. 

Easy Testing of NF/Icon-Support with `nerdfont.txt` file in Folder `jb_fonts`.

Fonts:

*   **Cascadia Code NF** (Microsoft - pretty Good NF and Emoji/Icon-Support)
*   **Firacode NF** (Nerdfonts.com)
*   **MesloLGS NF** (Support via Linux Community)
*   **Sauce Code Pro NF** (Nerdfonts.com - namely/originally: Source Code)

ToDo: 

Script for Installation - see [Gist - anthonyeden/FontInstallation.ps1](https://gist.github.com/anthonyeden/0088b07de8951403a643a8485af2709b)

```powershell
$SourceDir   = "<your path to fonts>"
$Destination = (New-Object -ComObject Shell.Application).Namespace(0x14)
Get-ChildItem -Path $SourceDir -Include '*.ttf','*.ttc','*.otf' -Recurse | ForEach {
    # Install font
    $Destination.CopyHere($_.FullName,0x10)
}
```

## 💻 Windows Terminal 

The standard Terminal Emulator for Windows Environments / OS.

Please feel free to use the following Snippets - 
but as always: don't just copy and paste - Think while using!

I like various Themes over the years. For these configurations i chose **Nord**.

### 🖼️ NordTheme

Put the followin into `schemes` Chapter of `settings.json` 
or use **One Half Dark** Standard-Theme.

The following Code shows also the Profile Entry for Windows PowerShell 
with a Background Pic provided in the jb_examples Folder.

```json
        "list": 
        [
            {
                "colorScheme": "nord",
                "font": 
                {
                    "face": "Cascadia Code NF",
                    "size": 13
                },
                "guid": "{61c54bbd-c2c6-5271-96e7-009a87ff44bf}",
                "hidden": false,
                "name": "Windows PowerShell",
                "startingDirectory": "%USERPROFILE%\\Documents\\git-projekte",
                "backgroundImage" : "%USERPROFILE%\\Documents\\WindowsPowerShell\\jb_examples\\Backgrounds\\nord_valley.png",
                "backgroundImageOpacity" : 0.10,
                "backgroundImageStretchMode" : "fill"
            },



    "schemes": 
    [
        {
            "background": "#2E3440",
            "black": "#2E3440",
            "blue": "#81A1C1",
            "brightBlack": "#4C566A",
            "brightBlue": "#81A1C1",
            "brightCyan": "#8FBCBB",
            "brightGreen": "#A3BE8C",
            "brightPurple": "#B48EAD",
            "brightRed": "#BF616A",
            "brightWhite": "#ECEFF4",
            "brightYellow": "#EBCB8B",
            "cursorColor": "#ECEFF4",
            "cyan": "#88C0D0",
            "foreground": "#D8DEE9",
            "green": "#A3BE8C",
            "name": "nord",
            "purple": "#B48EAD",
            "red": "#BF616A",
            "selectionBackground": "#ECEFF4",
            "white": "#E5E9F0",
            "yellow": "#EBCB8B"
        },

        ...

    ],    
```

Example Path: 

    C:\Users\USERNAME\AppData\Local\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState\settings.json

It is one of those Windows Apps paths - thank you very much MS.

## 📝 VS Code - Example settings.json 

Open with `Ctrl+Shift+P` - Preferences: Open User Settings (JSON)

See also Folder `jb_examples\VSCODE_USER_AppData-Roaming-Code-User`

```json
{
    "workbench.colorTheme": "Nord",
    "editor.fontFamily": "'Cascadia Code NF', 'Firacode NF', 'MesloLGS NF', Consolas, 'Courier New', monospace",
    "editor.fontSize": 16,
    "terminal.integrated.fontSize": 14,
    "editor.minimap.enabled": false,

    "update.mode": "none",
    "update.enableWindowsBackgroundUpdates": false,
    "telemetry.telemetryLevel": "off",

    "terminal.integrated.profiles.windows": {
        "PowerShell 7": {
            "source": "PowerShell",
            "icon": "terminal-powershell"
        },
        "PowerShell 5": {
            "path": "C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe",
            "icon": "terminal-powershell"
        },        
        "Command Prompt": {
            "path": [
                "${env:windir}\\System32\\cmd.exe"
            ],
            "args": [],
            "icon": "terminal-cmd"
        },
        "Git Bash": {
            "source": "Git Bash"
        }
    },
    "terminal.integrated.defaultProfile.windows": "PowerShell 5",
    
    "js-unicode-preview.languages": [
        "json",
        "jsonc",
        "javascript",
        "javascriptreact",
        "typescript",
        "typescriptreact"
    ]
}
```


Have fun - keep on PowerShell-ing

Joe Brandes - BS, 2025

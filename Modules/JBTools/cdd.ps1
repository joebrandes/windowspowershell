# Beispielfunktion cdd - Change Directory with Dialog
function cdd {
	$shell = New-Object -comObject "Shell.Application"
	$options = 0x51 # Nur Dateisystem-Ordner - inklusive Edit-Box
	$loc = $shell.BrowseForFolder(0, "Wohin soll es gehen?", $options)
if($loc) {Set-Location $loc.Self.Path}
}
